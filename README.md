# QScroller Test

For testing this bug: <https://bugs.kde.org/show_bug.cgi?id=425188>

It looks like QScroller gets corrupted QScreen pointers at some point, but Okular developers (where the bug was reported) are not able to reproduce it.

This test tries to reproduce the bug on the systems of the bug reporters.

## How to run this test

1. Needed software (Ubuntu package names):
   * build-essential
   * cmake (alternatively: qt5-qmake)
   * qtbase5-dev

   Probably useful debugging symbols: (Maybe with -dbg instead of -dbgsym)
   * libqt5widgets5-dbgsym
   * libqt5gui5-dbgsym

2. Checkout this project on your computer:
   * If you have `git` installed, you can do:
     ```bash
     $ git clone https://invent.kde.org/davidhurka/qscrollertest.git
     ```
   * If you don’t want to use `git`, you can look for the download button here:
     <https://invent.kde.org/davidhurka/qscrollertest>.

3. Open a terminal in the project folder.

4. Compile using CMake:
    ```bash
    $ mkdir build
    $ cd build
    $ cmake -G 'Unix Makefiles' ..
    $ make
    ```
   Or compile using qmake:
    ```bash
    $ mkdir build
    $ cd build
    $ qmake ..
    $ make
    ```

5. Run the test
    ```bash
    $ ./qscrollertest
    ```

6. Submit your test output to <https://bugs.kde.org/show_bug.cgi?id=425188> :)

## Problems:

Report problems or questions to <https://bugs.kde.org/show_bug.cgi?id=425188>,
I will be happy to help you.
