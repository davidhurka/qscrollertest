/*
 *   Copyright (C) 2020 David Hurka <david.hurka@mailbox.org>
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 */

#include <QDebug>
#include <QScreen>
#include <QScroller>
#include <QtTest>
#include <QWidget>

class QScrollerTest : public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testWidgetScreen();
    void testQScrollerOnNotShownWidget();
    void testQScrollerOnShownWidget();
};

void QScrollerTest::testWidgetScreen()
{
    QWidget testWidget;
    qDebug() << "Screen before showing widget:" << testWidget.screen();

    testWidget.show();
    qDebug() << "Screen after showing widget:" << testWidget.screen();
}

void QScrollerTest::testQScrollerOnNotShownWidget()
{
    QWidget testWidget;
    QScroller *testScroller = QScroller::scroller(&testWidget);

    qDebug() << "Scrolling a not shown widget.";
    testScroller->scrollTo(QPoint(50, 20), 0);
}

void QScrollerTest::testQScrollerOnShownWidget()
{
    QWidget testWidget;
    testWidget.show();
    QScroller *testScroller = QScroller::scroller(&testWidget);

    qDebug() << "Scrolling a shown widget.";
    testScroller->scrollTo(QPoint(50, 20), 0);
}

QTEST_MAIN(QScrollerTest)

#include "qscrollertest.moc"
